import std.algorithm : each, filter, map;
import std.array : join;
import std.file : dirEntries, readText, SpanMode;
import std.format : format;
import std.range : enumerate;
import std.stdio : writeln;
import std.string : indexOfNeither, replace, split;
import std.typecons : tuple;

void transformLstToErrorMessages(string lstFile)
{
    auto sourceFile = lstFile
        .replace("-", "/")
        .replace("./source", "source")
        .replace(".lst", ".d");
    lstFile
        .readText
        .split("\n")
        .enumerate(1)
        .filter!(line => line.value.split("|").length == 2)
        .map!((line) { auto h = line.value.split("|"); return tuple!("line", "count", "value")(line.index, h[0], h[1]);})
        .filter!(allLines => allLines.count == "0000000")
        .map!(uncoveredLine => "%s(%s,%s): Warning: Code not covered.".format(sourceFile, uncoveredLine.line, uncoveredLine.value.indexOfNeither(" ", 0)))
        .join("\n")
        .writeln;
}

void main(string[] args)
{
    ".".dirEntries("source-*.lst", SpanMode.shallow, false).each!transformLstToErrorMessages;
}
